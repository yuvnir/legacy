//Nav-bar
$(document).ready(function(){
$(window).scroll(function(){
	if($(this).scrollTop()>	135)
	{
		$(".bgg").addClass("redd")
	}
	else
	{
		$(".bgg").removeClass("redd")
	}
	if($(this).scrollTop()>	185)
	{
		$(".down").fadeIn()
	}
	else
	{
		$(".down").fadeOut()
	}
})
})

//AOS animation
AOS.init();

//preloader
{
	        $(window).on("load",function(){
          $(".loader-wrapper").fadeOut("slow");
        });
}

//Owl carousel
$(document).ready(function(){
  $(".owl-carousel").owlCarousel({
  	items:4,
    loop:true,
    margin:1,
    autoplay:true,
    autoplayTimeout:2000,
    autoplayHoverPause:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        480:{
            items:2,
            nav:true
        },
        600:{
            items:3,
            nav:true
        },
        1000:{
            items:4,
            nav:true,
            loop:false
        }
    }
  });
});



$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

$('[data-spy="scroll"]').each(function () {
  var $spy = $(this).scrollspy('refresh')
})